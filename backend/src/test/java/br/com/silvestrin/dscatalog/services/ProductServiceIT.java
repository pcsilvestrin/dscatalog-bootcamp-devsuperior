package br.com.silvestrin.dscatalog.services;

import br.com.silvestrin.dscatalog.dto.ProductDTO;
import br.com.silvestrin.dscatalog.repositories.ProductRepository;
import br.com.silvestrin.dscatalog.services.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional //Para cada teste que altera o banco é realizado o Rollback das transações no banco 02-37
public class ProductServiceIT {



    /*Testes de integração carregam todo o contexto da aplicação, pois para teste é utilizado informações reais*/
    @Autowired
    private ProductService service;

    @Autowired
    private ProductRepository repository;

    private Long existingId;
    private Long nonExistingId;
    private Long countTotalProducts;

    @BeforeEach
    void setUp() throws Exception {
        existingId = 1L;
        nonExistingId = 1000L;
        countTotalProducts = 25L;
    }

    @Test
    public void deleteShouldDeleteResourceWhenIdExists(){
        //Verifica se ao deletar um registro, o banco retorna a quantidade esperada (count = 24)
        service.delete(existingId);
        Assertions.assertEquals(countTotalProducts - 1, repository.count());
    }

    @Test
    public void deleteShouldThrowResourceNotFoundExceptionWhenIdDoesNotExists(){
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            service.delete(nonExistingId);
        });
    }

    @Test
    public void findAllPagedShouldReturnPageWhenPage0Size10(){
        PageRequest pageRequest = PageRequest.of(0, 10);

        Page<ProductDTO> result = service.findAllPaged(0L, "", pageRequest);

        Assertions.assertFalse(result.isEmpty()); //É esperado que retorne registros, negando o IsEmpty()
        Assertions.assertEquals(0, result.getNumber()); //Espera que o numero da Página seja "0"
        Assertions.assertEquals(10, result.getSize());  //Espera que tenha retornado 10 Registros
        Assertions.assertEquals(countTotalProducts, result.getTotalElements()); //Total de Registros no banco
    }

    @Test
    public void findAllPagedShouldReturnEmptyPageWhenPageDoesNotExist(){
        PageRequest pageRequest = PageRequest.of(50, 10);

        Page<ProductDTO> result = service.findAllPaged(0L, "", pageRequest);

        //É esperado que retorne uma Página Vazia, pois foi consultado a página 50
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void findAllPagedShouldReturnSortedPageWhenSortByName(){
        //Cria a página "0", esperando 10 registros, ordenados pelo Nome
        PageRequest pageRequest = PageRequest.of(0, 10, Sort.by("name"));

        Page<ProductDTO> result = service.findAllPaged(0L, "", pageRequest);

        Assertions.assertFalse(result.isEmpty());
        Assertions.assertEquals("Macbook Pro", result.getContent().get(0).getName()); //Compara se os registros foram ordenados
        Assertions.assertEquals("PC Gamer", result.getContent().get(1).getName());
        Assertions.assertEquals("PC Gamer Alfa", result.getContent().get(2).getName());
    }
}
