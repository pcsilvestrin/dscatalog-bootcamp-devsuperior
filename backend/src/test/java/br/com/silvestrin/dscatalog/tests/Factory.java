package br.com.silvestrin.dscatalog.tests;

import br.com.silvestrin.dscatalog.dto.ProductDTO;
import br.com.silvestrin.dscatalog.entities.Category;
import br.com.silvestrin.dscatalog.entities.Product;

import java.time.Instant;

public class Factory {

    public static Product createProduct(){
        Product product = new Product(
                "Phone",
                "Good Phone",
                800.0,
                "https://img.com/img.png",
                Instant.parse("2020-10-20T03:00:00Z")
        );

        product.getCategories().add(new Category(2L, "Eletronics"));
        return product;
    }

    public static ProductDTO createProductDTO(){
        Product product = createProduct();
        return  new ProductDTO(product, product.getCategories());
    }


    public static Category createCategory(){
        return new Category(2L, "Eletronics");
    }
}
