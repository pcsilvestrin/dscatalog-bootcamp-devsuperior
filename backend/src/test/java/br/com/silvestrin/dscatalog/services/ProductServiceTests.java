package br.com.silvestrin.dscatalog.services;

import static org.mockito.ArgumentMatchers.any;

import br.com.silvestrin.dscatalog.dto.ProductDTO;
import br.com.silvestrin.dscatalog.entities.Category;
import br.com.silvestrin.dscatalog.entities.Product;
import br.com.silvestrin.dscatalog.repositories.CategoryRepository;
import br.com.silvestrin.dscatalog.repositories.ProductRepository;
import br.com.silvestrin.dscatalog.services.exceptions.DataBaseException;
import br.com.silvestrin.dscatalog.services.exceptions.ResourceNotFoundException;
import br.com.silvestrin.dscatalog.tests.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class) //Anotação para teste de Unidade
public class ProductServiceTests {

    @InjectMocks
    private ProductService service;

    /*@Mock - Do pacote Mockito, utilizado nos testes de unidade simples, sem necessidade de carrregar o contexto a aplicação*/
    /*@MockBean - Do pacote JUnit 5, utilizado nos testes de unidade, porém, carrega o contexto da aplicação*/
    @Mock
    private ProductRepository repository;

    @Mock
    private CategoryRepository categoryRepository;

    private long existingId;
    private long nonExistingId;
    private long dependentId;
    private PageImpl<Product> page; //Representa um Pageble para simular nos testes
    private Product product;
    private ProductDTO productDTO;
    private Category category;

    @BeforeEach
    void setUp() throws Exception {
        existingId = 1L;
        nonExistingId = 1000L;
        dependentId = 4L;

        product    = Factory.createProduct();
        productDTO = Factory.createProductDTO();
        category   = Factory.createCategory();
        page = new PageImpl<>(List.of(product));

        //Ao chamar o método FindAll, com o Parametro Pageable, esperasse o retorno de um Pageble
        Mockito.when(repository.findAll((Pageable)any())).thenReturn(page);

        //Ao chamar o método Save, é esperado o retorno de um Produto
        Mockito.when(repository.save(any())).thenReturn(product);

        Mockito.when(repository.findById(existingId)).thenReturn(Optional.of(product));
        Mockito.when(repository.findById(nonExistingId)).thenReturn(Optional.empty());

        Mockito.when(repository.find(any(), any(), any())).thenReturn(page);

        Mockito.when(repository.getOne(existingId)).thenReturn(product);
        Mockito.when(repository.getOne(nonExistingId)).thenThrow(EntityNotFoundException.class);

        Mockito.when(categoryRepository.getOne(existingId)).thenReturn(category);
        Mockito.when(categoryRepository.getOne(nonExistingId)).thenThrow(EntityNotFoundException.class);

        //Ao chamar o deleteById com um ID EXISTENTE, esperasse que NÃO seja retornado NADA
        Mockito.doNothing().when(repository).deleteById(existingId);

        //Ao chamar o deleteById com um ID INEXISTENTE, esperasse que seja RETORNADO uma excessão do tipo EmptyResultDataAccessException
        Mockito.doThrow(EmptyResultDataAccessException.class).when(repository).deleteById(nonExistingId);

        Mockito.doThrow(DataIntegrityViolationException.class).when(repository).deleteById(dependentId);
    }


    @Test
    public void updateShouldReturnProductDTOWhenIdExists(){
        ProductDTO result = service.update(productDTO, existingId);
        Assertions.assertNotNull(result);
    }

    @Test
    public void updateShouldThrowResourceNotFoundExceptionWhenIdNotExists(){
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            service.update(productDTO, nonExistingId);
        });
    }

    @Test
    public void findByIdShouldReturnProductDTOWhenIdExists(){
        ProductDTO dto = service.findById(existingId);
        Assertions.assertNotNull(dto);
        Mockito.verify(repository).findById(existingId);
    }

    @Test
    public void findByIdShouldThrowResourceNotFoundExceptionWhenIdNotExists(){
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            service.findById(nonExistingId);
        });
        Mockito.verify(repository).findById(nonExistingId);
    }

    @Test
    public void findAllPagedShouldReturnPage(){
        Pageable pageable = PageRequest.of(0, 10);
        Page<ProductDTO> result = service.findAllPaged(0L, "", pageable);

        Assertions.assertNotNull(result);
    }


    @Test
    public void deleteShouldThrowDataBaseExceptionWhenDependentId(){
        Assertions.assertThrows(DataBaseException.class, () -> {
            service.delete(dependentId);
        });
        Mockito.verify(repository).deleteById(dependentId);
    }

    @Test
    public void deleteShouldThrowResourceNotFoundExceptionWhenIdDoesNotExists(){
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            service.delete(nonExistingId);
        });

        //Verifica se o método DeleteById foi executado na chamada acima
        Mockito.verify(repository).deleteById(nonExistingId);
    }

    @Test
    public void deleteShouldDoNothingWhenIdExists(){
        Assertions.assertDoesNotThrow(() -> {
            service.delete(existingId);
        });

        //Verifica se o método DeleteById foi executado na chamada acima
        Mockito.verify(repository).deleteById(existingId);

        //Espera que o "deleteById" seja chamado DUAS vezes
        //Mockito.verify(repository, Mockito.times(2)).deleteById(existingId);
    }

}
