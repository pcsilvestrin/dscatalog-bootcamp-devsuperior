package br.com.silvestrin.dscatalog.resources;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import br.com.silvestrin.dscatalog.dto.ProductDTO;
import br.com.silvestrin.dscatalog.services.ProductService;
import br.com.silvestrin.dscatalog.services.exceptions.DataBaseException;
import br.com.silvestrin.dscatalog.services.exceptions.ResourceNotFoundException;
import br.com.silvestrin.dscatalog.tests.Factory;
import br.com.silvestrin.dscatalog.tests.TokenUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductResourceTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService service;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TokenUtil tokenUtil;

    private ProductDTO productDTO;
    private PageImpl<ProductDTO> page;

    private long existingId;
    private long nonExistingId;
    private long dependentId;

    private String username;
    private String password;

    @BeforeEach
    void setUp() throws Exception{

        existingId = 1L;
        nonExistingId = 1000L;
        dependentId = 4L;

        username = "maria@gmail.com";
        password = "123456";

        productDTO = Factory.createProductDTO();
        page = new PageImpl<>(List.of(productDTO));

        when(service.insert(any())).thenReturn(productDTO);

        //Ao chamar o método FindAllPaged, passando qualquer argumento, é esperado uma página como retorno
        when(service.findAllPaged(any(), any(), any())).thenReturn(page);

        when(service.findById(existingId)).thenReturn(productDTO);
        when(service.findById(nonExistingId)).thenThrow(ResourceNotFoundException.class);

        when(service.update(any(), eq(existingId))).thenReturn(productDTO);
        when(service.update(any(), eq(nonExistingId))).thenThrow(ResourceNotFoundException.class);

        doNothing().when(service).delete(existingId); //Na deleção de um item existente, NÃO retornar nada
        doThrow(ResourceNotFoundException.class).when(service).delete(nonExistingId); //Na deleção de um item inexistente, espera a execessão
        doThrow(DataBaseException.class).when(service).delete(dependentId); //Na deleção de um item que possui integridade referencial
    }

    @Test
    public void insertShouldReturnProductDTO() throws Exception {
        String accessToken = tokenUtil.obtainAccessToken(mockMvc, username, password);

        String jsonBody = objectMapper.writeValueAsString(productDTO);

        ResultActions result =
                mockMvc.perform(post("/products")
                .header("Authorization", "Bearer "+accessToken)
                .content(jsonBody)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(status().isCreated());
        result.andExpect(jsonPath("$.name").exists());
        result.andExpect(jsonPath("$.description").exists());
    }

    @Test
    public void updateShouldReturnProductDTOWhenIdExists() throws Exception {
        String accessToken = tokenUtil.obtainAccessToken(mockMvc, username, password);

        //Converte um Obj Java em um JSON
        String jsonBody = objectMapper.writeValueAsString(productDTO);

        ResultActions result =
                mockMvc.perform(get("/products/{:id}", existingId)
                        .header("Authorization", "Bearer "+accessToken)
                        .content(jsonBody)  //Envia no corpo da Requisição
                        .contentType(MediaType.APPLICATION_JSON) //Tipo do objeto ENVIADO no corpo da Requisição
                        .accept(MediaType.APPLICATION_JSON)); //Tipo do objeto ESPERADO na resposta da Requisição

        result.andExpect(status().isOk());

        //Verifica se Existe o campo ID no corpo da Resposta
        //result.andExpect(jsonPath("$.id").exists());
        result.andExpect(jsonPath("$.name").exists());
        result.andExpect(jsonPath("$.description").exists());
    }

    @Test
    public void updateShouldReturnNotFoundWhenIdNonExists() throws Exception {
        String accessToken = tokenUtil.obtainAccessToken(mockMvc, username, password);

        //Converte um Obj Java em um JSON
        String jsonBody = objectMapper.writeValueAsString(productDTO);

        ResultActions result =
                mockMvc.perform(get("/products/{:id}", nonExistingId)
                        .header("Authorization", "Bearer "+accessToken)
                        .content(jsonBody)  //Envia no corpo da Requisição
                        .contentType(MediaType.APPLICATION_JSON) //Tipo do objeto ENVIADO no corpo da Requisição
                        .accept(MediaType.APPLICATION_JSON)); //Tipo do objeto ESPERADO na resposta da Requisição

        result.andExpect(status().isNotFound());
    }

    @Test
    public void findAllShouldReturnPage() throws Exception{
        //Realiza a requisição para o EndPoint "/products", esperando como resposta o código 200 (OK)
        //mockMvc.perform(get("/products")).andExpect(status().isOk());
        // OU

        ResultActions result =
                mockMvc.perform(get("/products")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk());
    }


    @Test
    public void findByIdShouldReturnProductWhenIdExists() throws Exception {
        ResultActions result =
                mockMvc.perform(get("/products/{:id}", existingId)
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk());

        //Verifica se Existe o campo ID no corpo da Resposta
        //result.andExpect(jsonPath("$.id").exists());
        result.andExpect(jsonPath("$.name").exists());
        result.andExpect(jsonPath("$.description").exists());
    }

    @Test
    public void findByIdShouldReturnNotFoundWhenIdNotExists() throws Exception {
        ResultActions result =
                mockMvc.perform(get("/products/{:id}", nonExistingId)
                        .accept(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNotFound());
    }

    @Test
    public void deleteShouldReturnNoContentWhenIdExist() throws Exception {
        String accessToken = tokenUtil.obtainAccessToken(mockMvc, username, password);

        ResultActions result =
                mockMvc.perform(delete("/products/{:id}", existingId)
                        .header("Authorization", "Bearer "+accessToken)
                        .accept(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNoContent());
    }

    @Test
    public void deleteShouldReturnNoContentWhenIdNoExist() throws Exception {
        String accessToken = tokenUtil.obtainAccessToken(mockMvc, username, password);

        ResultActions result =
                mockMvc.perform(delete("/products/{:id}", nonExistingId)
                        .header("Authorization", "Bearer "+accessToken)
                        .accept(MediaType.APPLICATION_JSON));

        result.andExpect(status().isNotFound());
    }
}
