package br.com.silvestrin.dscatalog.services.validation;

import br.com.silvestrin.dscatalog.dto.UserUpdateDTO;
import br.com.silvestrin.dscatalog.entities.User;
import br.com.silvestrin.dscatalog.repositories.UserRepository;
import br.com.silvestrin.dscatalog.resources.exceptions.FieldMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserUpdateValidator implements ConstraintValidator<UserUpdateValid, UserUpdateDTO> {
    /*Define que a classe UserUpdateDTO recebe e annotation de validação personalizada UserUpdateValid*/

    @Autowired
    private HttpServletRequest request; //Possibilita obter as informações da Requisição

    @Autowired
    private UserRepository repository;

    @Override
    public void initialize(UserUpdateValid ann) {
    }

    @Override
    public boolean isValid(UserUpdateDTO dto, ConstraintValidatorContext context) {

        //Carrega os Atributos passados na URL
        var uriVars = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        long userId = Long.parseLong(uriVars.get("id"));

        List<FieldMessage> list = new ArrayList<>();

        User user = repository.findByEmail(dto.getEmail());
        if (user != null && userId != user.getId()){
            list.add(new FieldMessage("email", "Email já informado para outro usuário!"));
        }

        for (FieldMessage e : list) {
            /*Percorre a lista de erros personalizada e alimenta a lista de erros do Bean Validation*/
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }
        return list.isEmpty();
    }
}