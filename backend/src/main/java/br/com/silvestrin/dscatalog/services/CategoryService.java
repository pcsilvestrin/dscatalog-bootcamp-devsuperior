package br.com.silvestrin.dscatalog.services;

import br.com.silvestrin.dscatalog.dto.CategoryDTO;
import br.com.silvestrin.dscatalog.entities.Category;
import br.com.silvestrin.dscatalog.repositories.CategoryRepository;
import br.com.silvestrin.dscatalog.services.exceptions.DataBaseException;
import br.com.silvestrin.dscatalog.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    /*Cria uma Transação com o banco de dados e define que ela é apenas     */
    /*de leitura, não gerando versionamento de informações no banco de Dados*/
    @Transactional(readOnly = true)
    public Page<CategoryDTO> findAllPaged(Pageable pageble){
        Page<Category> list = categoryRepository.findAll(pageble);
        return list.map(category -> new CategoryDTO(category));
    }

    @Transactional(readOnly = true)
    public CategoryDTO findById(Long id){
        Optional<Category> obj = categoryRepository.findById(id);

        /*Caso não seja encontrado a Category pelo ID, será disparado a exceção EntityNotFoundException*/
        /*que será tratada na camada Resource*/
        Category entity = obj.orElseThrow(() -> new ResourceNotFoundException("Categoria não encontrada!"));
        return new CategoryDTO(entity);
    }

    @Transactional
    public CategoryDTO insert(CategoryDTO dto){
        Category entity = new Category();
        entity.setName(dto.getName());
        entity = categoryRepository.save(entity);
        return new CategoryDTO(entity);
    }

    @Transactional
    public CategoryDTO update(CategoryDTO dto, Long id){
        try {
            //O getOne(), instancia um objeto com o ID para que ao atualizar o banco de dados
            //seja atualizado o registro que tenha o mesmo id, isso para que não seja realizado duas consultas
            //no banco de dados
            Category entity = categoryRepository.getOne(id);
            entity.setName(dto.getName());
            entity = categoryRepository.save(entity);
            return new CategoryDTO(entity);
        } catch (EntityNotFoundException e) {
            //Ao atualizar o registro, caso não encontre o mesmo Id, é tratado a exceção gerada pelo JPA
            throw new ResourceNotFoundException("Id inexistente: " + id);
        }
    }

    public void delete(Long id){
        try {
            categoryRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            //Se não existir o registro com o ID
            throw new ResourceNotFoundException("Id inexistente: " + id);
        } catch (DataIntegrityViolationException e) {
            //Caso ocorra exceção de integridade referencial
            throw new DataBaseException("Violação de Integridade");
        }
    }
}
