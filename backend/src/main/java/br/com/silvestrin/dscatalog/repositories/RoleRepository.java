package br.com.silvestrin.dscatalog.repositories;

import br.com.silvestrin.dscatalog.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
