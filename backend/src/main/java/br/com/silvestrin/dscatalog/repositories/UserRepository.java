package br.com.silvestrin.dscatalog.repositories;

import br.com.silvestrin.dscatalog.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

}
