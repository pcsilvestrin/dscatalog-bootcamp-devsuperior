package br.com.silvestrin.dscatalog.services.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.silvestrin.dscatalog.dto.UserInsertDTO;
import br.com.silvestrin.dscatalog.entities.User;
import br.com.silvestrin.dscatalog.repositories.UserRepository;
import br.com.silvestrin.dscatalog.resources.exceptions.FieldMessage;
import org.springframework.beans.factory.annotation.Autowired;

public class UserInsertValidator implements ConstraintValidator<UserInsertValid, UserInsertDTO> {
    /*Define que a classe UserInsertDTO recebe e annotation de validação personalizada UserInsertValid*/

    @Autowired
    private UserRepository repository;

    @Override
    public void initialize(UserInsertValid ann) {
    }

    @Override
    public boolean isValid(UserInsertDTO dto, ConstraintValidatorContext context) {

        List<FieldMessage> list = new ArrayList<>();

        User user = repository.findByEmail(dto.getEmail());
        if (user != null){
            list.add(new FieldMessage("email", "Email já informado!"));
        }

        for (FieldMessage e : list) {
            /*Percorre a lista de erros personalizada e alimenta a lista de erros do Bean Validation*/
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }
        return list.isEmpty();
    }
}