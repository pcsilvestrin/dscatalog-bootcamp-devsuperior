package br.com.silvestrin.dscatalog.services;

import br.com.silvestrin.dscatalog.dto.CategoryDTO;
import br.com.silvestrin.dscatalog.dto.ProductDTO;
import br.com.silvestrin.dscatalog.entities.Category;
import br.com.silvestrin.dscatalog.entities.Product;
import br.com.silvestrin.dscatalog.repositories.CategoryRepository;
import br.com.silvestrin.dscatalog.repositories.ProductRepository;
import br.com.silvestrin.dscatalog.services.exceptions.DataBaseException;
import br.com.silvestrin.dscatalog.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Transactional(readOnly = true)
    public Page<ProductDTO> findAllPaged(Long categoryId, String name, Pageable pageable){

        //É carregado em memória um Obj do tipo Category, quando o ID do parâmetro for ZERO (padrão),
        //não pode ser realizado a consulta ".getOne()", caso contrário ocorre erro
        List<Category> categories = (categoryId == 0) ? null : Arrays.asList(categoryRepository.getOne(categoryId));

        Page<Product> list = repository.find(categories, name, pageable);

        //Primeiro Instância em Memória as Categorias dos Produtos para não ocorrer o Problema da "N mais 1 Consultas" 05-30
        repository.findProductWithCategories(list.getContent()); //Passa por parâmetro a lista de Produtos que está no Page
        return list.map(product -> new ProductDTO(product, product.getCategories()));
    }

    @Transactional(readOnly = true)
    public ProductDTO findById(Long id){
        Optional<Product> obj = repository.findById(id);
        Product entity = obj.orElseThrow(() -> new ResourceNotFoundException("Produto não encontrada!"));
        return new ProductDTO(entity, entity.getCategories());
    }

    @Transactional
    public ProductDTO insert(ProductDTO dto){
        Product entity = new Product();
        copyDtoToEntity(dto, entity);
        entity = repository.save(entity);
        return new ProductDTO(entity);
    }

    @Transactional
    public ProductDTO update(ProductDTO dto, Long id){
        try {
            Product entity = repository.getOne(id);
            copyDtoToEntity(dto, entity);
            entity = repository.save(entity);
            return new ProductDTO(entity);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException("Id inexistente: " + id);
        }
    }

    public void delete(Long id){
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            //Se não existir o registro com o ID
            throw new ResourceNotFoundException("Id inexistente: " + id);
        } catch (DataIntegrityViolationException e) {
            //Caso ocorra exceção de integridade referencial
            throw new DataBaseException("Violação de Integridade");
        }
    }

    private void copyDtoToEntity(ProductDTO dto, Product entity){
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setDate(dto.getDate());
        entity.setImgUrl(dto.getImgUrl());
        entity.setPrice(dto.getPrice());

        /*Limpa a lista de categorias e popula com o que está na lista do DTO*/
        entity.getCategories().clear();
        for (CategoryDTO catDto : dto.getCategories()){
            Category category = categoryRepository.getOne(catDto.getId());
            entity.getCategories().add(category);
        }
    }
}
