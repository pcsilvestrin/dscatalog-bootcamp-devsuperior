package br.com.silvestrin.dscatalog.dto;

import br.com.silvestrin.dscatalog.entities.Role;

import java.io.Serializable;

public class RoleDTO implements Serializable {
    private static  final long serialVersionUID = 1L;

    private Long id;
    private String authorithy;

    public RoleDTO(){

    }

    public RoleDTO(Long id, String authorithy) {
        this.id = id;
        this.authorithy = authorithy;
    }

    public RoleDTO(Role role) {
        this.id = role.getId();
        this.authorithy = role.getAuthority();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthorithy() {
        return authorithy;
    }

    public void setAuthorithy(String authorithy) {
        this.authorithy = authorithy;
    }
}
