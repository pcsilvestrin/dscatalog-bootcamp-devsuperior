package br.com.silvestrin.dscatalog.services.exceptions;

public class ResourceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /* Exceção criada para tratar no Service, quando a consulta findById não encontra a entidade */
    /* sem esse tratamento, para o usuário é retornado status 500                                */
    public ResourceNotFoundException(String msg){
        super(msg);
    }
}
