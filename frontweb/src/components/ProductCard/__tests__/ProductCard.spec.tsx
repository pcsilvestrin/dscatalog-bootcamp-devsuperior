import { render, screen } from '@testing-library/react';
import { Product } from 'types/product';
import ProductCard from "..";

test('should render ProductCard', () => {
    
    const product : Product = {
        name : "Produto Teste",
        price: 1555.21,
        imgUrl: "https://teste.com"
    } as Product; //Para não ser preciso informar todos os atributos

    render(
        <ProductCard product={product} />
    )

    expect(screen.getByText(product.name)).toBeInTheDocument();    
    expect(screen.getByAltText(product.name)).toBeInTheDocument();    

    expect(screen.getByText("R$")).toBeInTheDocument();
    expect(screen.getByText("1.555,21")).toBeInTheDocument();    
    
});