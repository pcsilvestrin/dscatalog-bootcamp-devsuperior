import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Form from "../Form";
import history from 'utils/history';
import { Router, useParams } from 'react-router-dom';
import { productResponse, server } from "./fixtures";
import selectEvent from "react-select-event";
import { ToastContainer } from 'react-toastify';

//Antes dos testes, carrega o Server Mockado
beforeAll(() => server.listen());

//Após cada teste, reseta as configurações
afterEach(() => server.resetHandlers());

//Após os testes, o Server é fechado
afterAll(() => server.close());

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useParams: jest.fn()
}));

describe('Product form create tests', () => {

    beforeEach(() => {
        (useParams as jest.Mock).mockReturnValue({
            productId: 'create'
        })
    });

    test('should show toast and redirect when submit form correctly', async () => {
        render(
            <Router history={history}>
                <ToastContainer />
                <Form />
            </Router>
        );

        //Captura os elementos do Form
        const nameInput = screen.getByTestId("name");
        const priceInput = screen.getByTestId("price");
        const imgUrlInput = screen.getByTestId("imgUrl");
        const descriptionInput = screen.getByTestId("description");
        const categoriesInput = screen.getByLabelText("Categorias");

        //Captura o Botão Salvar
        const submitButton = screen.getByRole('button', { name: /salvar/i });

        //Simula a seleção de elementos no campo Cetegory
        await selectEvent.select(categoriesInput, ['Eletrônicos', 'Computadores']);

        //Simulando a digitação nos campos 
        userEvent.type(nameInput, 'Computador');
        userEvent.type(priceInput, '5000.12');
        userEvent.type(imgUrlInput, 'https://static.wikia.nocookie.net/dublagem/images/e/ec/Fud%C3%AAncio.png/revision/latest?cb=20210620010726&path-prefix=pt-br');
        userEvent.type(descriptionInput, 'Computador Top');

        //Executa o evento Click do botão Salvar
        userEvent.click(submitButton);

        await waitFor(() => {
            const toastElement = screen.getByText('Produto cadastrado com sucesso');
            expect(toastElement).toBeInTheDocument();
        });

        //Após salvar o Produto, é verificado para qual endereço a Aplicação foi redirecionada
        expect(history.location.pathname).toEqual('/admin/products');
    });

    test('should show 5 validation messages when just clicking submit', async () => {

        render(
            <Router history={history}>
                <Form />
            </Router>
        );

        const submitButton = screen.getByRole('button', { name: /salvar/i })

        //Realiza o Salvar sem informar valor para os campos
        userEvent.click(submitButton);

        await waitFor(() => {
            const messages = screen.getAllByText('Campo obrigatório');
            expect(messages).toHaveLength(5); //Espera que a mensagem acima seja apresentada 5 vezes
        });
    });

    test('should clear validation messages when filling out the form correctly', async () => {

        render(
            <Router history={history}>
                <Form />
            </Router>
        );

        const submitButton = screen.getByRole('button', { name: /salvar/i })

        userEvent.click(submitButton);

        //Salva os o Form sem informar os campos, espera q ocorre 5 mensagens de Campo Obrigatório
        await waitFor(() => {
            const messages = screen.getAllByText('Campo obrigatório');
            expect(messages).toHaveLength(5);
        });

        //Captura os elementos do Form
        const nameInput = screen.getByTestId("name");
        const priceInput = screen.getByTestId("price");
        const imgUrlInput = screen.getByTestId("imgUrl");
        const descriptionInput = screen.getByTestId("description");
        const categoriesInput = screen.getByLabelText("Categorias");

        //Simula a seleção de elementos no campo Cetegory
        await selectEvent.select(categoriesInput, ['Eletrônicos', 'Computadores']);

        //Simulando a digitação nos campos 
        userEvent.type(nameInput, 'Computador');
        userEvent.type(priceInput, '5000.12');
        userEvent.type(imgUrlInput, 'https://static.wikia.nocookie.net/dublagem/images/e/ec/Fud%C3%AAncio.png/revision/latest?cb=20210620010726&path-prefix=pt-br');
        userEvent.type(descriptionInput, 'Computador Top');

        //Executa o evento Click do botão Salvar
        userEvent.click(submitButton);

        //É esperado que não seja apresentado as mensagens de Campo Obrigatório
        await waitFor(() => {
            const messages = screen.queryAllByText('Campo obrigatório');
            expect(messages).toHaveLength(0);
        });

        expect(history.location.pathname).toEqual('/admin/products');
    });
});


describe('Product form update tests', () => {

    beforeEach(() => {
        (useParams as jest.Mock).mockReturnValue({
            productId: '2'
        })
    });

    test('should show toast and redirect when submit form correctly', async () => {

        render(
            <Router history={history}>
                <ToastContainer />
                <Form />
            </Router>
        );
    
        await waitFor(() => {
            const nameInput = screen.getByTestId("name");
            const priceInput = screen.getByTestId("price");
            const imgUrlInput = screen.getByTestId("imgUrl");
            const descriptionInput = screen.getByTestId("description");

            const formElement = screen.getByTestId("form");
            
            expect(nameInput).toHaveValue(productResponse.name);
            expect(priceInput).toHaveValue(String(productResponse.price));
            expect(imgUrlInput).toHaveValue(productResponse.imgUrl);
            expect(descriptionInput).toHaveValue(productResponse.description);

            const ids = productResponse.categories.map(x => String(x.id));
            expect(formElement).toHaveFormValues({categories: ids});
        });
        
        const submitButton = screen.getByRole('button', { name: /salvar/i})

        userEvent.click(submitButton);

        await waitFor(() => {
            const toastElement = screen.getByText('Produto cadastrado com sucesso');
            expect(toastElement).toBeInTheDocument();
        });

        expect(history.location.pathname).toEqual('/admin/products');
    });
});
