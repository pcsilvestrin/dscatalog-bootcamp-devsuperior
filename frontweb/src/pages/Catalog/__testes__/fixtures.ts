import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { BASE_URL } from 'utils/requests';

const findAllResponse = {
    "content": [
        {
            "id": 1,
            "name": "The Lord of the Rings xxx",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "price": 90.58,
            "imgUrl": "https://raw.githubusercontent.com/devsuperior/dscatalog-resources/master/backend/img/1-big.jpg",
            "date": null,
            "categories": [
                {
                    "id": 2,
                    "name": "Eletrônicos"
                }
            ]
        },
        {
            "id": 31,
            "name": "teste teste",
            "description": "sfsdfsdf",
            "price": 1101.35,
            "imgUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT2fkCYvW1rZdT0gi0ujcMJAx3dfuFiP6yvd0Gwmk4BZYDf2GqmMjhzMR7X8ySj-imrI3g&usqp=CAU",
            "date": null,
            "categories": [
                {
                    "id": 1,
                    "name": "Livros"
                },
                {
                    "id": 2,
                    "name": "Eletrônicos"
                },
                {
                    "id": 3,
                    "name": "Computadores"
                }
            ]
        },
        {
            "id": 32,
            "name": "teste Goku",
            "description": "teste",
            "price": 100.0,
            "imgUrl": "https://listasnerds.com.br/wp-content/uploads/2022/01/goku-dragon-ball-9-850x560.jpg",
            "date": null,
            "categories": [
                {
                    "id": 1,
                    "name": "Livros"
                }
            ]
        },
        {
            "id": 28,
            "name": "Teste",
            "description": "teste teste",
            "price": 20000.0,
            "imgUrl": null,
            "date": null,
            "categories": [
                {
                    "id": 1,
                    "name": "Livros"
                }
            ]
        },
        {
            "id": 2,
            "name": "Smart TV",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "price": 2190.32,
            "imgUrl": "https://raw.githubusercontent.com/devsuperior/dscatalog-resources/master/backend/img/2-big.jpg",
            "date": null,
            "categories": [
                {
                    "id": 1,
                    "name": "Livros"
                },
                {
                    "id": 3,
                    "name": "Computadores"
                }
            ]
        },
        {
            "id": 5,
            "name": "Rails for Dummies",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "price": 100.99,
            "imgUrl": "https://raw.githubusercontent.com/devsuperior/dscatalog-resources/master/backend/img/5-big.jpg",
            "date": "2020-07-14T10:00:00Z",
            "categories": [
                {
                    "id": 2,
                    "name": "Eletrônicos"
                }
            ]
        },
        {
            "id": 10,
            "name": "PC Gamer Y",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "price": 1700.32,
            "imgUrl": "https://raw.githubusercontent.com/devsuperior/dscatalog-resources/master/backend/img/10-big.jpg",
            "date": null,
            "categories": [
                {
                    "id": 3,
                    "name": "Computadores"
                }
            ]
        },
        {
            "id": 7,
            "name": "PC Gamer X",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "price": 1350.0,
            "imgUrl": "https://raw.githubusercontent.com/devsuperior/dscatalog-resources/master/backend/img/7-big.jpg",
            "date": "2020-07-14T10:00:00Z",
            "categories": [
                {
                    "id": 3,
                    "name": "Computadores"
                }
            ]
        },
        {
            "id": 15,
            "name": "PC Gamer Weed xxxx",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "price": 2200.0,
            "imgUrl": "https://raw.githubusercontent.com/devsuperior/dscatalog-resources/master/backend/img/15-big.jpg",
            "date": null,
            "categories": [
                {
                    "id": 3,
                    "name": "Computadores"
                }
            ]
        },
        {
            "id": 21,
            "name": "PC Gamer Tx",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "price": 1680.0,
            "imgUrl": "https://raw.githubusercontent.com/devsuperior/dscatalog-resources/master/backend/img/21-big.jpg",
            "date": "2020-07-14T10:00:00Z",
            "categories": [
                {
                    "id": 3,
                    "name": "Computadores"
                }
            ]
        },
        {
            "id": 17,
            "name": "PC Gamer Turbo",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "price": 1280.0,
            "imgUrl": "https://raw.githubusercontent.com/devsuperior/dscatalog-resources/master/backend/img/17-big.jpg",
            "date": "2020-07-14T10:00:00Z",
            "categories": [
                {
                    "id": 3,
                    "name": "Computadores"
                }
            ]
        },
        {
            "id": 20,
            "name": "PC Gamer Tr",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "price": 1650.0,
            "imgUrl": "https://raw.githubusercontent.com/devsuperior/dscatalog-resources/master/backend/img/20-big.jpg",
            "date": null,
            "categories": [
                {
                    "id": 3,
                    "name": "Computadores"
                }
            ]
        }
    ],
    "pageable": {
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "pageNumber": 0,
        "pageSize": 12,
        "offset": 0,
        "paged": true,
        "unpaged": false
    },
    "last": false,
    "totalPages": 3,
    "totalElements": 28,
    "first": true,
    "sort": {
        "sorted": true,
        "unsorted": false,
        "empty": false
    },
    "number": 0,
    "numberOfElements": 12,
    "size": 12,
    "empty": false

}

export const server = setupServer(

    //Simular a consulta de Produtos na API, porém retorna para testes o JSON acima
    rest.get(`${BASE_URL}/products`, (req, res, ctx) => {
        return res(
            ctx.status(200),
            ctx.json(findAllResponse)
        );
    })
);