import { render, screen, waitFor } from "@testing-library/react";
import Catalog from "..";
import history from "utils/history";
import { Router } from "react-router-dom";
import { server } from './fixtures';

//Antes dos testes, carrega o Server Mockado
beforeAll(() => server.listen());

//Após cada teste, reseta as configurações
afterEach(() => server.resetHandlers());

//Após os testes, o Server é fechado
afterAll(() => server.close());

test('should render Catalog with products', async () => {

    render(
        <Router history={history}>
            <Catalog />        
        </Router>
    );

    //screen.debug();

    expect(screen.getByText('Catálogo de Produtos')).toBeInTheDocument();

    //Aguarda a renderização e espera um elemento com o texto 'teste teste'
    await waitFor(() => {
        expect(screen.getByText('teste teste')).toBeInTheDocument();
    });
});
