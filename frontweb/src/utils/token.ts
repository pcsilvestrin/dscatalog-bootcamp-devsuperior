import jwtDecode from "jwt-decode";
import { Role } from "types/role";
import { getAuthData } from "./storage";

export type TokenData = {
    exp: number;
    user_name: string;
    authorities: Role[];
};


//Função responsável por Decodificar o Token salvo no Storage
export const getTokenData = (): TokenData | undefined => {
    try {
        return jwtDecode(getAuthData().access_token) as TokenData;
    } catch (error) {
        return undefined;
    }
};