import { Role } from 'types/role';
import { getTokenData } from './token';

export const isAuthenticated = (): boolean => {
  const tokenData = getTokenData();

  //Verifica se existe o Token e se o mesmo não está expirado
  return tokenData && tokenData.exp * 1000 > Date.now() ? true : false;
};

export const hasAnyRoles = (roles: Role[]): boolean => {
  if (roles.length === 0) {
    return true;
  }

  const tokenData = getTokenData();

  if (tokenData !== undefined) {
    //Verifica se para o usuário logado, existe a Role necessária para acessar determinada página
    return roles.some((role) => tokenData.authorities.includes(role));
  }

  /*if (tokenData !== undefined) {
      for (var iFor = 0; iFor < roles.length; iFor++) {
        if (tokenData.authorities.includes(roles[iFor])) {
          return true;
        }
      }
    }*/

  return false;
};
