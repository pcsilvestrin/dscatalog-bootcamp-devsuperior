import axios, { AxiosRequestConfig } from 'axios';
import qs from 'qs';
import { getAuthData } from 'utils/storage';
import history from './history';

export const BASE_URL =
  process.env.REAC_APP_BACKEND_URL ??
  //'https://127.0.0.1:8080';
  'https://aula-implantacao-dscatalog-dev.herokuapp.com';

export const CLIENT_ID = process.env.REAC_APP_CLIENT_ID ?? 'dscatalog';
export const CLIENT_SECRET =
  process.env.REAC_APP_CLIENT_SECRET ?? 'dscatalog123';

//Concatena as chaves e converte para Base64
const basicHeader = () =>
  `Basic ${window.btoa(CLIENT_ID + ':' + CLIENT_SECRET)}`;

type LoginData = {
  username: string;
  password: string;
};

export const requestBackendLogin = (loginData: LoginData) => {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    Authorization: basicHeader(),
  };

  const data = qs.stringify({
    //Converte um objeto em url code
    ...loginData,
    grant_type: 'password',
  });

  return axios({
    method: 'POST',
    baseURL: BASE_URL,
    url: '/oauth/token',
    data,
    headers,
  });
};

export const requestBackend = (config: AxiosRequestConfig) => {
  const headers = config.withCredentials
    ? {
        ...config.headers, //Mantém o Header informado na Requisição
        Authorization: 'Bearer ' + getAuthData().access_token,
      }
    : config.headers;

  return axios({ ...config, baseURL: BASE_URL, headers });
};

//Captura antes do Envio da Requisição
axios.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

//Captura a Resposta da requisição e trata de acordo com statuscode
axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    //Caso seja retornado erro referente a permissão do usuário
    if (error.response.status === 401 || error.response.status === 403) {
      history.push('/admin/auth');
    }
    return Promise.reject(error);
  }
);
